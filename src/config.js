export const serverUrl =
  process.env.NODE_ENV === 'production'
    ? window.location.origin
    : 'http://localhost:8080';
