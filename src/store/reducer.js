import {
  searchParamChanged,
  dataFetchFulfilled,
  fieldsPickerRequested,
  fieldsPickerCloseRequested,
  fieldVisibilityChanged,
  pageChanged,
} from './actions';

const initialState = {
  visibleFields: [
    { name: 'DRG Definition', propName: 'drgDefinition', value: false },
    { name: 'Provider Id', propName: 'id', value: true },
    { name: 'Provider Name', propName: 'name', value: true },
    { name: 'Provider Street Address', propName: 'street', value: false },
    { name: 'Provider City', propName: 'city', value: true },
    { name: 'Provider State', propName: 'state', value: true },
    { name: 'Provider Zip Code', propName: 'zipCode', value: false },
    {
      name: 'Hospital Referral Region Description',
      propName: 'hospitalReferralRegionDescription',
      value: false,
    },
    { name: 'Total Discharges', propName: 'totalDischarges', value: true },
    {
      name: 'Average Covered Charges',
      propName: 'averageCoveredCharges',
      value: true,
    },
    {
      name: 'Average Total Payments',
      propName: 'averageTotalPayments',
      value: true,
    },
    {
      name: 'Average Medicare Payments',
      propName: 'averageMedicarePayments',
      value: true,
    },
  ],
  searchControlValues: {
    max_discharges: '',
    min_discharges: '',
    max_average_covered_charges: '',
    min_average_covered_charges: '',
    max_average_medicare_payments: '',
    min_average_medicare_payments: '',
    state: '',
  },
  fieldsPickerVisible: false,
  fetchInProgress: false,
  pagination: {
    skip: 0,
    limit: 20,
    from: 0,
    to: 0,
    totalCount: 0,
  },
  data: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case searchParamChanged.type: {
      const { propName, value } = action;
      return {
        ...state,
        searchControlValues: {
          ...state.searchControlValues,
          [propName]: value,
        },
        fetchInProgress: true,
        pagination: {
          ...state.pagination,
          skip: 0,
        },
      };
    }

    case dataFetchFulfilled.type: {
      const { data, totalCount } = action;
      return {
        ...state,
        fetchInProgress: false,
        pagination: {
          ...state.pagination,
          totalCount,
          from: state.pagination.skip + 1,
          to:
            totalCount < state.pagination.skip + state.pagination.limit
              ? totalCount
              : state.pagination.skip + state.pagination.limit,
        },
        data,
      };
    }

    case fieldsPickerRequested.type:
      return {
        ...state,
        fieldsPickerVisible: true,
      };

    case fieldsPickerCloseRequested.type:
      return {
        ...state,
        fieldsPickerVisible: false,
      };

    case fieldVisibilityChanged.type: {
      const { propName, value } = action;
      return {
        ...state,
        visibleFields: state.visibleFields.map(item => {
          if (item.propName !== propName) return item;
          return {
            ...item,
            value,
          };
        }),
      };
    }

    case pageChanged.type: {
      const { next } = action;
      const prevPageOnFirstPage = !next && state.pagination.skip === 0;
      const nextPageOnLastPage =
        next &&
        state.pagination.skip + state.pagination.limit >=
          state.pagination.totalCount;

      if (prevPageOnFirstPage || nextPageOnLastPage) return state;

      const skip =
        state.pagination.skip + state.pagination.limit * (next ? 1 : -1);
      const from = skip + 1;
      let to = skip + state.pagination.limit;
      if (to > state.pagination.totalCount) {
        to = state.pagination.totalCount;
      }
      return {
        ...state,
        pagination: {
          ...state.pagination,
          skip,
          from,
          to,
        },
      };
    }

    default:
      return state;
  }
};

export default reducer;
