import nock from 'nock';
import createStore from './';

const makeInitialState = part =>
  Object.assign(
    {
      visibleFields: [],
      searchControlValues: {},
      fieldsPickerVisible: false,
      fetchInProgress: false,
      pagination: {
        skip: 0,
        limit: 20,
      },
      data: [],
    },
    part,
  );

describe('Store', () => {
  let apiMock = null;
  beforeAll(() => {
    apiMock = nock('http://localhost:8080')
      .get('/api/providers')
      .reply(200, {})
      .persist();
  });
  afterAll(() => {
    apiMock.persist(false);
  });
  describe('on search field change', () => {
    it('updates search value', () => {
      const store = createStore(makeInitialState({}));
      store.dispatch({
        type: 'SEARCH_PARAM_CHANGED',
        propName: 'max_discharges',
        value: 10,
      });
      expect(store.getState().searchControlValues).toHaveProperty(
        'max_discharges',
        10,
      );
    });
    it('marks fetching in progress', () => {
      const store = createStore(makeInitialState({}));
      store.dispatch({
        type: 'SEARCH_PARAM_CHANGED',
        propName: 'max_discharges',
        value: 10,
      });
      expect(store.getState()).toHaveProperty('fetchInProgress', true);
    });
    it('resets api pagination', () => {
      const store = createStore(makeInitialState({}));
      store.dispatch({
        type: 'SEARCH_PARAM_CHANGED',
        propName: 'max_discharges',
        value: 10,
      });
      expect(store.getState().pagination).toHaveProperty('skip', 0);
    });
  });
});
