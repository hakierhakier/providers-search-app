export const searchParamChanged = ({ propName, value }) => ({
  type: searchParamChanged.type,
  propName,
  value,
});
searchParamChanged.type = 'SEARCH_PARAM_CHANGED';

export const dataFetchFulfilled = ({ data, totalCount }) => ({
  type: dataFetchFulfilled.type,
  data,
  totalCount,
});
dataFetchFulfilled.type = 'DATA_FETCH_FULFILLED';

export const fieldsPickerRequested = () => ({
  type: fieldsPickerRequested.type,
});
fieldsPickerRequested.type = 'FIELDS_PICKER_REQUESTED';

export const fieldsPickerCloseRequested = () => ({
  type: fieldsPickerCloseRequested.type,
});
fieldsPickerCloseRequested.type = 'FIELDS_PICKER_CLOSE_REQUESTED';

export const fieldVisibilityChanged = ({ propName, value }) => ({
  type: fieldVisibilityChanged.type,
  propName,
  value,
});
fieldVisibilityChanged.type = 'FIELD_VISIBILITY_REQUESTED';

export const pageChanged = ({ next }) => ({
  type: pageChanged.type,
  next,
});
pageChanged.type = 'PAGE_REQUESTED';
