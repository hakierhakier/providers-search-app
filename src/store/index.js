import {
  createStore as defaultCreateStore,
  compose as defaultCompose,
  applyMiddleware,
} from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducer from './reducer';
import saga from './saga';

const createStore = initialState => {
  let compose = defaultCompose;
  if (process.env.NODE_ENV !== 'production') {
    compose = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  }

  const sagaMiddleware = createSagaMiddleware();
  const store = defaultCreateStore(
    reducer,
    initialState,
    compose(applyMiddleware(sagaMiddleware)),
  );
  sagaMiddleware.run(saga);

  return store;
};

export default createStore;
