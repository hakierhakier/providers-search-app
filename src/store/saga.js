import {
  fork,
  takeLatest,
  debounce,
  call,
  select,
  put,
} from 'redux-saga/effects';
import {
  searchParamChanged,
  dataFetchFulfilled,
  fieldVisibilityChanged,
  pageChanged,
} from './actions';
import { getProviders } from '../api';

function* fetchProvidersData() {
  const { visibleFields, searchControlValues, pagination } = yield select();
  const fields = visibleFields
    .filter(({ value }) => value)
    .map(({ propName }) => propName);
  const { skip, limit } = pagination;
  const { data, totalCount } = yield call(getProviders, {
    ...searchControlValues,
    fields,
    skip,
    limit,
  });
  yield put(dataFetchFulfilled({ data, totalCount }));
}

function* listenForSearchParamsChange() {
  yield debounce(400, searchParamChanged.type, fetchProvidersData);
}

function* listenForFieldsVisibilityChange() {
  yield takeLatest(fieldVisibilityChanged.type, fetchProvidersData);
}

function* listenForPageChange() {
  yield takeLatest(pageChanged.type, fetchProvidersData);
}

function* rootSaga() {
  yield fork(listenForSearchParamsChange);
  yield fork(listenForFieldsVisibilityChange);
  yield fork(listenForPageChange);
  yield call(fetchProvidersData);
}

export default rootSaga;
