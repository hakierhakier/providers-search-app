import { connect } from 'react-redux';
import Providers from '../views/Providers';
import {
  searchParamChanged,
  fieldsPickerRequested,
  fieldsPickerCloseRequested,
  fieldVisibilityChanged,
  pageChanged,
} from '../store/actions';

const mapState = ({
  visibleFields,
  searchControlValues,
  fieldsPickerVisible,
  fetchInProgress,
  pagination,
  data,
}) => ({
  visibleFields,
  searchControlValues,
  fieldsPickerVisible,
  fetchInProgress,
  pagination,
  data,
});

const mapDispatch = {
  searchParamChanged,
  fieldsPickerRequested,
  fieldsPickerCloseRequested,
  fieldVisibilityChanged,
  pageChanged,
};

export default connect(
  mapState,
  mapDispatch,
)(Providers);
