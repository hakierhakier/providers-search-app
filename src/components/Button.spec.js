import React from 'react';
import { shallow } from 'enzyme';
import Button from './Button';

describe('<Button />', () => {
  it('renders correctly', () => {
    const wrapper = shallow(<Button />);
    expect(wrapper).toHaveLength(1);
  });
  it('renders provided children', () => {
    const wrapper = shallow(
      <Button>
        <span>a child</span>
      </Button>,
    );
    expect(wrapper.contains(<span>a child</span>)).toBeTruthy();
  });
  it('invokes onClick callback on click event', () => {
    const onClick = jest.fn();
    const wrapper = shallow(<Button onClick={onClick} />);
    wrapper.simulate('click');
    expect(onClick).toHaveBeenCalled();
  });
});
