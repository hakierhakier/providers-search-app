import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { css } from 'emotion';
import { v4 } from 'uuid';

const styles = {
  main: css`
    box-sizing: border-box;
    display: block;
    margin: 0;
    width: 100%;
  `,
};

class Input extends PureComponent {
  id = `input.${v4()}`;
  render() {
    const { type, label, value, onChange } = this.props;
    return (
      <div>
        {label && <label htmlFor={this.id}>{label}</label>}
        <input
          id={this.id}
          className={styles.main}
          type={type}
          value={value}
          onChange={onChange}
        />
      </div>
    );
  }
}

Input.propTypes = {
  type: PropTypes.oneOf(['text', 'number']),
  label: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
};

Input.defaultProps = {
  type: 'text',
  value: '',
  onChange: () => {},
};

export default Input;
