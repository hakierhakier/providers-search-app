import React, { PureComponent, Fragment } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { css } from 'emotion';

const styles = {
  overlay: css`
    align-items: center;
    background: rgba(100, 100, 100, 0.6);
    bottom: 0;
    display: flex;
    justify-content: center;
    left: 0;
    position: fixed;
    right: 0;
    top: 0;
  `,
  overlayContent: css`
    background: white;
    margin: 20px;
    padding: 10px;
  `,
};

class Modal extends PureComponent {
  componentDidMount() {
    document.body.appendChild(this.element);
  }

  componentWillUnmount() {
    document.body.removeChild(this.element);
  }

  stopPropagation = e => {
    e.stopPropagation();
  };

  element = document.createElement('div');

  render() {
    const { children, onCloseRequest } = this.props;
    return (
      <Fragment>
        {ReactDOM.createPortal(
          <div className={styles.overlay} onClick={onCloseRequest}>
            <div
              className={styles.overlayContent}
              onClick={this.stopPropagation}
            >
              {children}
            </div>
          </div>,
          this.element,
        )}
      </Fragment>
    );
  }
}

Modal.propTypes = {
  children: PropTypes.node,
  onCloseRequest: PropTypes.func,
};

export default Modal;
