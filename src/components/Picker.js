import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';

class Picker extends PureComponent {
  makeChangeHandler = propName => ({ target: { checked } }) => {
    this.props.onChange({ propName, value: checked });
  };

  render() {
    const { options } = this.props;
    return (
      <Fragment>
        {options.map(({ name, propName, value }) => (
          <div key={`picker-option.${name}`}>
            <label>
              <input
                type="checkbox"
                checked={value}
                onChange={this.makeChangeHandler(propName)}
              />
              {name}
            </label>
          </div>
        ))}
      </Fragment>
    );
  }
}

Picker.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({ name: PropTypes.string, value: PropTypes.bool }),
  ),
  onChange: PropTypes.func,
};

Picker.defaultProps = {
  onChange: () => {},
  options: [],
};

export default Picker;
