import { serverUrl } from './config';

export const getProviders = async searchParamsData => {
  const parseValue = value =>
    Array.isArray(value)
      ? value.map(encodeURIComponent).join(',')
      : encodeURIComponent(value);
  const searchParams = Object.entries(searchParamsData)
    .filter(([prop, value]) => Boolean(value))
    .map(([prop, value]) => `${prop}=${parseValue(value)}`)
    .join('&');
  const response = await fetch(`${serverUrl}/api/providers?${searchParams}`);
  return await response.json();
};
