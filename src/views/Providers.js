import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { css, cx } from 'emotion';
import { Button, Input, Modal, Picker } from '../components';

const styles = {
  searchArea: css`
    display: flex;
    flex-wrap: wrap;
    width: 100%;
  `,
  searchControl: css`
    padding: 4px 6px;
  `,
  searchControlGrown: css`
    flex-grow: 1;
  `,
  searchControlPaltry: css`
    align-items: center;
    display: flex;
  `,
  results: css`
    width: 100%;
  `,
  resultsFaded: `
    opacity: 0.6;
  `,
  resultsRow: css`
    &:nth-child(odd) {
      background: rgba(100, 100, 100, 0.1);
    }
  `,
  resultsCell: css`
    &:not(:last-child) {
      border-right: 1px solid rgba(100, 100, 100, 0.1);
    }
  `,
  noResults: css`
    text-align: center;
  `,
};

const fields = [
  { label: 'Max discharges', type: 'number', propName: 'max_discharges' },
  { label: 'Min discharges', type: 'number', propName: 'min_discharges' },
  {
    label: 'Max average covered charges',
    type: 'number',
    propName: 'max_average_covered_charges',
  },
  {
    label: 'Min average covered charges',
    type: 'number',
    propName: 'min_average_covered_charges',
  },
  {
    label: 'Max average medicare payments',
    type: 'number',
    propName: 'max_average_medicare_payments',
  },
  {
    label: 'Min average medicare payments',
    type: 'number',
    propName: 'min_average_medicare_payments',
  },
  { label: 'State', type: 'text', propName: 'state' },
];

const makeChangeHandler = (callback, propName) => ({ target: { value } }) => {
  callback({ propName, value });
};

const Providers = ({
  visibleFields,
  searchControlValues,
  fieldsPickerVisible,
  fetchInProgress,
  pagination,
  data,
  searchParamChanged,
  fieldsPickerRequested,
  fieldsPickerCloseRequested,
  fieldVisibilityChanged,
  pageChanged,
}) => (
  <div>
    <div className={styles.searchArea}>
      {fields.map(({ label, type, propName }) => (
        <div
          key={`search-control.${label}`}
          className={cx(styles.searchControl, styles.searchControlGrown)}
        >
          <Input
            label={label}
            type={type}
            value={searchControlValues[propName]}
            onChange={makeChangeHandler(searchParamChanged, propName)}
          />
        </div>
      ))}
      <div className={cx(styles.searchControl, styles.searchControlPaltry)}>
        <Button onClick={fieldsPickerRequested}>Visible columns</Button>
        {fieldsPickerVisible && (
          <Modal onCloseRequest={fieldsPickerCloseRequested}>
            <Picker options={visibleFields} onChange={fieldVisibilityChanged} />
          </Modal>
        )}
      </div>
    </div>
    <hr />
    {!data.length && <div className={styles.noResults}>No data</div>}
    {Boolean(data.length) && (
      <Fragment>
        <div>
          <Button onClick={() => pageChanged({ next: false })}>{'<'}</Button>
          <Button onClick={() => pageChanged({ next: true })}>{'>'}</Button>
          {` ${pagination.from} - ${pagination.to} of ${pagination.totalCount}`}
        </div>
        <table
          className={cx(styles.results, {
            [styles.resultsFaded]: fetchInProgress,
          })}
        >
          <thead>
            <tr>
              {visibleFields
                .filter(({ value }) => value)
                .map(({ name }) => (
                  <th
                    key={`table-column-header.${name}`}
                    className={styles.resultsCell}
                  >
                    {name}
                  </th>
                ))}
            </tr>
          </thead>
          <tbody>
            {data.map(item => (
              <tr key={`table-row.${item._id}`} className={styles.resultsRow}>
                {visibleFields
                  .filter(({ value }) => value)
                  .map(({ propName }) => (
                    <td
                      key={`table-cell.${propName}`}
                      className={styles.resultsCell}
                    >
                      {item[propName]}
                    </td>
                  ))}
              </tr>
            ))}
          </tbody>
        </table>
      </Fragment>
    )}
  </div>
);

Providers.propTypes = {
  visibleFields: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      propName: PropTypes.string,
      value: PropTypes.bool,
    }),
  ),
  searchControlValues: PropTypes.shape({
    max_discharges: PropTypes.string,
    min_discharges: PropTypes.string,
    max_average_covered_charges: PropTypes.string,
    min_average_covered_charges: PropTypes.string,
    max_average_medicare_payments: PropTypes.string,
    min_average_medicare_payments: PropTypes.string,
    state: PropTypes.string,
  }),
  fieldsPickerVisible: PropTypes.bool,
  fetchInProgress: PropTypes.bool,
  pagination: PropTypes.shape({
    from: PropTypes.number,
    to: PropTypes.number,
    totalCount: PropTypes.number,
  }),
  data: PropTypes.arrayOf(
    PropTypes.shape({
      drgDefinition: PropTypes.string,
      id: PropTypes.string,
      name: PropTypes.string,
      street: PropTypes.string,
      city: PropTypes.string,
      state: PropTypes.string,
      zipCode: PropTypes.string,
      hospitalReferralRegionDescription: PropTypes.string,
      totalDischarges: PropTypes.number,
      averageCoveredCharges: PropTypes.number,
      averageTotalPayments: PropTypes.number,
      averageMedicarePayments: PropTypes.number,
    }),
  ),
  searchParamChanged: PropTypes.func,
  fieldsPickerRequested: PropTypes.func,
  fieldsPickerCloseRequested: PropTypes.func,
  fieldVisibilityChanged: PropTypes.func,
  pageChanged: PropTypes.func,
};

Providers.defaultProps = {
  visibleFields: [],
  searchControlValues: {},
  fieldsPickerVisible: false,
  fetchInProgress: false,
  pagination: {
    from: 0,
    to: 0,
    totalCount: 0,
  },
  data: [],
  searchParamChanged: () => {},
  fieldsPickerRequested: () => {},
  fieldsPickerCloseRequested: () => {},
  fieldVisibilityChanged: () => {},
  pageChanged: () => {},
};

export default Providers;
