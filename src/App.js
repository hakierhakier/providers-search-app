import React, { PureComponent } from 'react';
import { Provider } from 'react-redux';
import createStore from './store';
import Providers from './containers/Providers';

class App extends PureComponent {
  store = createStore();

  render() {
    return (
      <Provider store={this.store}>
        <Providers />
      </Provider>
    );
  }
}

export default App;
