const { readFileSync } = require('fs');
const { resolve } = require('path');
const parse = require('csv-parse/lib/sync');
const mongoose = require('mongoose');
const ProvidersModel = require('../src/providers-model');

(async () => {
  const csvData = readFileSync(resolve(__dirname, '../data.csv'));
  const data = parse(csvData, {
    columns: true,
    skip_empty_lines: true,
  });

  await mongoose.connect(
    'mongodb://heroku_wtk19ds2:6vr4d06786826g1mg81imkfpd4@ds021299.mlab.com:21299/heroku_wtk19ds2',
    { useNewUrlParser: true },
  );
  console.info('Connected');
  const collections = await mongoose.connection.db.listCollections().toArray();
  if (collections.find(({ name }) => name === 'providers')) {
    await mongoose.connection.dropCollection('providers');
    console.info('Providers collection removed');
  }

  console.info('Inserting data');

  let currentItem = 0;
  const progressInterval = setInterval(() => {
    const progress = Math.round((currentItem / data.length) * 10000) / 100;
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(`${progress}%`);
  }, 100);

  await data.reduce(
    (acc, row, index) =>
      acc.then(async () => {
        await ProvidersModel.create({
          drgDefinition: row['DRG Definition'],
          id: row['Provider Id'],
          name: row['Provider Name'],
          street: row['Provider Street Address'],
          city: row['Provider City'],
          state: row['Provider State'],
          zipCode: row['Provider Zip Code'],
          hospitalReferralRegionDescription:
            row['Hospital Referral Region Description'],
          totalDischarges: Number(row[' Total Discharges ']),
          averageCoveredCharges: Number(
            row[' Average Covered Charges '].slice(1),
          ),
          averageTotalPayments: Number(
            row[' Average Total Payments '].slice(1),
          ),
          averageMedicarePayments: Number(
            row['Average Medicare Payments'].slice(1),
          ),
        });
        currentItem = index;
      }),
    Promise.resolve(),
  );
  clearInterval(progressInterval);
  process.stdout.clearLine();
  process.stdout.cursorTo(0);
  process.stdout.write('100%\n');
  console.info('Data have been put in the DB');
  await mongoose.connection.close();
})();
