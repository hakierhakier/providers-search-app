const mongoose = require('mongoose');

module.exports = mongoose.model(
  'Providers',
  mongoose.Schema(
    {
      drgDefinition: { type: String, required: true },
      id: { type: String, required: true },
      name: { type: String, required: true },
      street: { type: String, required: true },
      city: { type: String, required: true },
      state: { type: String, required: true },
      zipCode: { type: String, required: true },
      hospitalReferralRegionDescription: { type: String, required: true },
      totalDischarges: { type: Number, required: true },
      averageCoveredCharges: { type: Number, required: true },
      averageTotalPayments: { type: Number, required: true },
      averageMedicarePayments: { type: Number, required: true },
    },
    { id: false },
  ),
  'providers',
);
