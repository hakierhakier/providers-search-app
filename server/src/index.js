const mongoose = require('mongoose');
const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const { port, mongoUrl } = require('./config');
const ProvidersModel = require('./providers-model');

const app = express();
const mongoConnection = mongoose.connect(mongoUrl, { useNewUrlParser: true });

app.use(morgan('tiny'));
app.use(express.json());
if (process.env.NODE_ENV !== 'production') {
  app.use(cors());
}
if (process.env.NODE_ENV === 'production') {
  console.log('PROD');
  app.use(express.static('../build'));
}
app.use((req, res, next) => mongoConnection.then(() => next()));

app.get('/api/providers', async (req, res) => {
  const {
    max_discharges,
    min_discharges,
    max_average_covered_charges,
    min_average_covered_charges,
    max_average_medicare_payments,
    min_average_medicare_payments,
    state,
    fields,
    skip = 0,
    limit = 10,
  } = req.query;

  const getQuery = () => {
    const query = ProvidersModel.find();

    if (min_discharges) {
      query.where('totalDischarges').gte(Number(min_discharges));
    }
    if (max_discharges) {
      query.where('totalDischarges').lte(Number(max_discharges));
    }
    if (min_average_covered_charges) {
      query
        .where('averageCoveredCharges')
        .gte(Number(min_average_covered_charges));
    }
    if (max_average_covered_charges) {
      query
        .where('averageCoveredCharges')
        .lte(Number(max_average_covered_charges));
    }
    if (min_average_medicare_payments) {
      query
        .where('averageMedicarePayments')
        .gte(Number(min_average_medicare_payments));
    }
    if (max_average_medicare_payments) {
      query
        .where('averageMedicarePayments')
        .lte(Number(max_average_medicare_payments));
    }
    if (state) {
      query.where({ state });
    }
    if (fields) {
      query.select(fields.split(',').join(' '));
    }
    return query;
  };

  const totalCount = await getQuery()
    .countDocuments()
    .exec();
  const data = await getQuery()
    .skip(Number(skip))
    .limit(Number(limit))
    .exec();

  res.send({
    totalCount,
    data,
  });
});

app.listen(port, () => {
  console.info(`Listeneing on port ${port}`);
});
