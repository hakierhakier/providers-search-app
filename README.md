# Providers search app

This repository contains client and server apps.

Application can be accessed under url: [https://powerful-mountain-17791.herokuapp.com/](https://powerful-mountain-17791.herokuapp.com/)

## Dependencies installation

```bash
yarn
cd server
yarn
```

## Running

### Server

```bash
cd server
docker-compose up -d
yarn insert-data
yarn start
```

### Client

```bash
yarn start
```

### Tests

```bash
yarn test
```

## Technical description

### Server
 - tech stack: Express/MongoDB
 - exposes single endpoint /api/providers
 - endpoint supports pagination
 - in production mode serves static client files

### Client
 - tech stack: React/Redux/Saga/Emotion
 - separation of view (react) and logic (redux)
 - app state designed to be easily consumable by view
 - CSS in JS
